# Script qui ajoute des utilisateurs � l'Active Directory en utilisant un fichier CSV

Import-Module ActiveDirectory # Importation du module AD

$users = Import-Csv -Path "./AD Users.csv" -Delimiter ";" # Importation du fichier CSV


# Fonction qui ajoute un utilisateur � l'AD
function New-UserAD {
    New-ADUser `
                -EmployeeID $id `
                -Name $nom_complet `
                -SamAccountName $SAM `
                -UserPrincipalName $SAM `
                -DisplayName $name `
                -GivenName $prenom `
                -Surname $nom `
                -City $site `
                -AccountPassword (convertTo-SecureString $mot_de_passe -AsPlainText -Force) `
                -Enabled $true `
                -Path $ou
}

# Cr�ation des OU
New-ADOrganizationalUnite -Name Developpement -Path "OU=Developpement,DC=Somily,DC=local"
New-ADOrganizationalUnite -Name Production -Path "OU=Production,DC=Somily,DC=local"
New-ADOrganizationalUnite -Name Support -Path "OU=Support,DC=Somily,DC=local"


foreach ($user in $users) # Pour chaque utilisateurs
{ # D�finition des variables

    $id = $user.id
    $nom_complet = $user.Nom + " " + $user.Prenom
    $nom = $user.Nom
    $prenom = $user.Prenom
	$premiere_lettre_prenom = $prenom.substring(0, 1)
	$SAM = $premiere_lettre_prenom + "." + $nom
    $email = "$SAM@somily.fr"
	$mot_de_passe= "Admin@2023"
    $site = $user.Site

# V�rification de l'�quipe
    switch($user.Equipe){
        "developpement" { $ou = "OU=Developpement,DC=Somily,DC=local" }
        "production" { $ou = "OU=Production,DC=Somily,DC=local" }
        "support" { $ou = "OU=Support,DC=Somily,DC=local" }
        default { $ou = null }
    }
	
	if($ou -ne $null){
		if (Get-ADUser -Filter {SAmAccountName -eq $SAM}){
            Write-Warning "$nom_complet existe d�j� dans l'AD"
        } else {
            New-UserAD
			echo "Utilisateur ajout� : $nom_complet"
		}
	}	
}
