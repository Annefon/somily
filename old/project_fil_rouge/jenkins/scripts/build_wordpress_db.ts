pipeline {
    agent any
  
    stages {
      stage('Build') {
        steps {
          // Clone your Git repository, if needed
          // git 'https://github.com/your/repo.git'
  
          // Build the Docker images, if needed
          // sh 'docker build -t my_image .'
  
          // Create the necessary directories, if needed
          sh 'mkdir -p /home/formation/project_fil_rouge/docker/wordpress_db/data'
          
          // Run the Ansible playbook to launch the Docker Compose if there is any
          sh 'ansible-playbook /home/formation/project_fil_rouge/ansible/playbooks/wordpress_db/wordpress_db.yml'
        }
      }
    }
  }
  