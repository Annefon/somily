# Readme-Projet-Fil-Rouge-Groupe 4

![logo](img/logo_somily.png)



## Maquette du projet (Architecture fonctionnelle du projet (un POC))

## Cadre du projet 

### Présentation de l'entreprise Somily 

Somily est une nouvelle société dont le logo est représenté ci-dessus. Cette société offre des services de consulting et de formation dans le domaine des technologies de l'information. Elle compte en son sein 20 collaborateurs répartis dans 3 sites stratégiques au Centre et à l'Ouest de la France (Paris, Rennes, Nantes).

- Paris gère principalement les serveurs et les réseaux de l'entreprise (équipe de production) 
- Rennes est spécialisé dans le développement et compte des membres d'autres équipes (équipe de développement)
- Nantes gère principalement la demande des clients et les incidents (équipe de support)

![organisation](img/Organisation_somilyv2.png)
### Présentation succincte du projet

Le but du projet est la création d'un site web (blog) pour présenter les services et les offres de formation de Somily. La visée est d'augmenter la visibilité de l'entreprise auprès du public et de clients potentiels et de se démarquer de la concurrence.

Spécification du site web :
- hébergé en interne
- accessible depuis l'extérieur
- sécurisé
- disponible 24h/24 et 7j/7
- maintenable et évolutif

A cela s'ajoute que l'entreprise souhaite :
- un outil de gestion de projets et de centralisation de code source des applications : GitLab
- que les administrateurs système en interne (l'équipe de production) aient une vue d'ensemble de l'état des serveurs et des services.


## Architecture globale

![architecture](img/architecture3.png)



## Déploiement de l'infrastructure de l'entreprise Somily



## Prérequis

- VMware ESXI
- Accès à internet Accès à Internet pour télécharger les packages nécessaires
- Accès en lecture/écriture aux fichiers du playbook Ansible sur la machine virtuelle où ils se trouvent.


## Comment utiliser ce projet

1. Cloner ce repository Git
2. Se placer à la racine du projet


## Connexion SSH entre les machines virtuelles

Les machines virtuelles peuvent communiquer entre elles grâce à des connexions SSH. Les adresses IP des machines virtuelles sont :

- 137.74.106.203 pour la machine virtuelle Jenkins
- 141.94.6.204 pour la machine virtuelle Ansible
- 137.74.106.202 pour la machine virtuelle Nginx
- 137.74.106.208 pour la machine virtuelle Docker Wordpress et MySQL
- 137.74.106.209 pour la machine virtuelle Docker Gitlab (OMNIBUS)
- 141.94.6.205 pour la machine virtuelle Docker Prometheus et Graphana
- 141.94.6.206 pour la machine virtuelle Docker Elastic Search et Kibana

Pour se connecter à une machine virtuelle depuis une autre machine virtuelle, il suffit de se connecter en utilisant l'utilisateur 'utilisateur' et le mot de passe 'mot_de_passe'. Par exemple, pour se connecter à la machine virtuelle Jenkins depuis la machine virtuelle Flask, il faut exécuter la commande suivante :

```bash
ssh utilisateur@141.94.6.204
```

## Procedure générale

1. Cloner le dépôt Git sur la machine virtuelle Ansible

```bash
git clone https://github.com/<nom_utilisateur>/project_fil_rouge.git
```

2. Aller à la racine du projet

```bash
cd racine/du/projet/project_fil_rouge/
```

## Installation de Ansible

Ansible est un outil open-source d'automatisation de la gestion de configuration, de déploiement d'applications et d'orchestration. Il permet de définir des tâches à exécuter sur des serveurs distants de manière simple et reproductible, grâce à une syntaxe déclarative et un agent léger installé sur les machines cibles.

### Pré-requis d'installation Ansible

Pour pouvoir connecter et gérer les machines virtuelles distantes avec Ansible, vous devez vous assurer que votre serveur Ansible répond aux prérequis suivants :

- Présence de Python 2.6 ou version ultérieure sur la machine qui administre Ansible
- Installation d'Ansible sur la machine qui administre Ansible
- Connexion SSH avec les machines distantes
- Fichier d'inventaire répertoriant toutes les machines

Aucun agent n'est installé sur les machines distantes car Ansible n'a pas besoin de communiquer avec des agents pour gérer les machines distantes.

### Verification de l'installation de Python
Si vous utilisez Ubuntu, vous pouvez vérifier si Python est installé en utilisant l'une des méthodes suivantes :

- whois python (si nécessaire, installez-le via sudo apt install whois)
- sudo find / -name python
- python --version ou python3 --version

Si Python n'est pas installé, exécutez la commande suivante :

```bash
sudo apt install python3-pip
```

Pour vérifier l'installation, utilisez la commande suivante :

```bash
pip install ansible
```

Ensuite, vous devez modifier le fichier .bashrc.

Vérifiez d'abord la présence de ce fichier avec la commande ls -l, puis ouvrez-le avec un éditeur de texte (par exemple en tapant nano .bashrc).

Ajoutez la ligne suivante à la fin du fichier :

```bash 
export PATH=$PATH:/home/formation/.local/bin
```

Remplacez formation par votre nom d'utilisateur.

Vérifiez que la modification a été prise en compte avec la commande cat .bashrc, puis mettez à jour sans arrêter votre poste avec la commande source .bashrc. Vérifiez que la variable est bien prise en compte avec la commande echo $PATH.

Pour vérifier que Ansible est bien installé, utilisez la commande suivante dans votre terminal :

```bash
ansible 
ansible --version # pour connaitre la version d'ansible installée
```
### Connexion SSH avec les machines distantes

Pour établir une connexion SSH avec les machines distantes, vous devez échanger une clé publique.

Commencez par générer une clé avec la commande suivante :

```bash 
ssh-keygen
```

Ensuite, récupérez toutes les adresses IP publiques des machines distantes et utilisez la commande suivante pour chaque adresse IP :

```bash 
ssh-copy-id -i ./.ssh/id_rsa.pub <utilisateur>@<adresse IP public>
```

Pour vérifier que votre poste Ansible est bien connecté aux postes distants, utilisez la commande suivante :

Pour une adresse à la fois :

```bash
ansible -m ping -u <utilisateur> <adresse IP>
```

Pour tous les postes enregistrés dans l'inventaire :

```bash
ansible -m ping -u <utilisateur> -i ./inventaire all
```

## Installation de Jenkins

Jenkins est un outil open-source d'intégration continue et de déploiement continu (CI/CD) qui permet de faciliter la gestion de la construction, du test et du déploiement d'applications. Il intègre des fonctionnalités telles que le suivi de versions de code, l'intégration avec des outils de gestion de configuration tels que Git, ainsi que des fonctionnalités de planification de tâches et de notification en temps réel. Jenkins peut être utilisé en conjonction avec des outils tels qu'Ansible pour faciliter la mise en place de pipelines d'intégration et de déploiement continus.

Ouvrir une session SSH sur la machine virtuelle Jenkins en utilisant la commande:

```bash
ssh username@jenkins-IP-address
```

Installer le serveur Jenkins en suivant les instructions fournies dans la documentation officielle de Jenkins. 
Utiliser les commande suivante pour télécharger et installer Jenkins:

Pour ajouter la clé publique:

```bash
wget -q -O - https://pkg.jenkins.io/debian-stable/jenkins.io.key | sudo apt-key add 
```

Pour ajouter la source:

```bash
sudo sh -c 'echo deb https://pkg.jenkins.io/debian-stable binary/ > /etc/apt/sources.list.d/jenkins.list'
```

Pour mettre à jour les packages:

```bash
sudo apt-get update
```

Pour installer Jenkins:

```bash
sudo apt-get install jenkins
```

Lancer le service Jenkins en utilisant la commande suivante : 

```bash
sudo systemctl start jenkins
```

Vérifier que le service Jenkins est en cours d'exécution en utilisant la commande suivante : 

```bash
sudo systemctl status jenkins
```

## Configuration de Jenkins

1. Ouvrir un navigateur Web et accéder à l'adresse IP de la machine virtuelle Jenkins en utilisant le port 8080 (ex : http://adresse_IP-jenkins:8080).
2. Configurer Jenkins en suivant les instructions fournies dans la documentation officielle de Jenkins.
3. Installer les plugins nécessaires pour l'exécution du playbook Ansible en utilisant le Gestionnaire de plugins de Jenkins.
4. Créer un projet Jenkins pour l'exécution du playbook Ansible en suivant les instructions fournies dans la documentation officielle de Jenkins. Dans le projet, vous pouvez spécifier le répertoire du playbook Ansible et les machines virtuelles cibles.

## Exécution du playbook Ansible sur Jenkins

Les scripts Jenkins sont dans le chemin : ./project_fil_rouge/jenkins/scripts

1. Pour copier les scripts Jenkins sur la machine Jenkins et créer un projet "Fil_Rouge", vous pouvez exécuter les commandes suivantes :

```bash
# Copier les scripts Jenkins sur la machine Jenkins
scp -r ./projet_fil_rouge/jenkins/scripts jenkins@<adresse_IP_Jenkins>:/home/jenkins
```

2. Ouvrir un navigateur Web et accéder à l'adresse IP de la machine virtuelle Jenkins en utilisant le port 8080 (ex : http://jenkins-IP-address:8080).

3. Créer un nouveau projet Jenkins "Fil_Rouge" en utilisant l'interface graphique de Jenkins

4. Exécuter le projet Jenkins "Fil_Rouge" pour déployer le playbook Ansible

5. Vérifier que le playbook Ansible est en cours d'exécution en utilisant la commande suivante sur les machines virtuelles cibles : sudo systemctl status nginx

```bash
ssh utilisateur@<adresse_IP_wordpress>
sudo systemctl status nginx
ssh <utilisateur_mysql>@<adresse_IP_mysql>
sudo systemctl status mysql
```

Nota.Bene : Il est également possible d'executer le playbook Ansible sans passer par Jenkins, en utilisant la commande ansible-playbook depuis la machine de contrôle où Ansible est installé. Pour cela, vous devez vous connecter à la machine de contrôle, puis exécuter la commande "ansible-playbook /chemin/du/playbook/nomplaybook.yml -i /chemin/de/l'inventory/inventory.ini". Cela déclenchera le processus de déploiement automatisé et configurera les machines virtuelles avec les services associés en fonction du contenu du playbook Ansible.

Notez que pour exécuter cette commande, la connexion SSH entre la machine de contrôle et les machines virtuelles cibles doit être configurée comme décrit précédemment dans ce document.

## Déploiement de Nginx sur une VM Ubuntu via un playbook Ansible gérér par Jenkins

Nginx est un serveur web open-source qui peut également être utilisé comme proxy inverse, équilibrage de charge et cache de contenu. Il est apprécié pour sa performance élevée, sa stabilité et sa flexibilité dans la configuration.

Cette partie du README explique comment déployer Nginx en mode reverse proxy sur une VM Ubuntu en utilisant Ansible. Le playbook opensslcertificate.yml est utilisé pour générer les certificats SSL nécessaires pour le déploiement. Le playbook Ansible start_nginx.yml est utilisé pour installer Nginx et configurer le proxy Nginx sécurisé. Notez que le playbook start_nginxyml nécessite l'execution préalable du playbook opensslcertificate.yml

## Procédure

Assurez vous d'avoir accès en lecture/écriture aux fichiers de configuration Nginx sur la machine virtuelle où Nginx est installé.

### Modifier le fichier start_nginx.yml selon les besoins. Les variables qui peuvent être modifiées sont :

- nginx_listen_port: port sur lequel Nginx écoutera les requêtes HTTP.
- nginx_server_name: nom de domaine que Nginx utilisera pour servir le contenu.
- nginx_ssl_certificate: chemin du certificat SSL pour le serveur.
- nginx_ssl_certificate_key: chemin de la clé privée associée au certificat SSL.
- nginx_ssl_certificate_csr: chemin du CSR pour le serveur.
- allowed_hosts: liste des hôtes autorisés à accéder au serveur.

### Modifier le fichier opensslcertificate.yml selon les besoins. Les variables qui peuvent être modifiées sont: 

- server_hostname: nom de domaine du serveur.
- key_size: taille de la clé privée. 
- passphrase: mot de passe pour protéger la clé privée.
- key_type: type de la clé privée (RSA, DSA, ECC, Ed25519, Ed448, X25519, X448).
- country_name: nom du pays.
- email_address: adresse e-mail de l'administrateur.
- organization_name: nom de l'organisation.
- Exécuter le playbook opensslcertificate.yml sur la machine virtuelle

### Configuration de Nginx

Le fichier nginx.conf.j2 présent dans le répertoire ./project_fil_rouge/ansible/playbooks/nginx/conf/nginx.conf.j2 permet de configurer un serveur Nginx pour proxyfier différentes applications web.

#### Configuration des applications

es différentes applications sont configurées à travers les sections location du fichier.

- /ansible : proxyfie l'application Ansible qui est accessible à l'adresse:
  http://141.94.6.204:80 
- /jenkins : proxyfie l'application Jenkins qui est accessible à l'adresse:
  http://137.74.106.203:80
- /wordpress : proxyfie l'application Wordpress qui est accessible à l'adresse:
  http://137.74.106.208:8080
- /mysql : proxyfie l'application MySQL qui est accessible à l'adresse:
  http://137.74.106.208:3306
- /gitlab : proxyfie l'application GitLab qui est accessible à l'adresse:
  http://137.74.106.209:80
- /prometheus : proxyfie l'application Prometheus qui est accessible à l'adresse:
  http://141.94.6.205:9090
- /graphana : proxyfie l'application Graphana qui est accessible à l'adresse:
  http://141.94.6.205:3000
- /elasticsearch : proxyfie l'application Elasticsearch qui est accessible à l'adresse:
  http://141.94.6.206:9200
- /kibana : proxyfie l'application Kibana qui est accessible à l'adresse:
  http://141.94.6.206:5601



Pour chaque section, on définit l'adresse IP et le port de l'application à travers l'option proxy_pass. On configure également des headers pour le proxy 

N.B : Il est important de vérifier la configuration de chaque application pour s'assurer que les ports et les adresses IP sont corrects.

#### Configuration SSL

Le serveur est configuré pour utiliser un certificat SSL, qui est défini à travers les options ssl_certificate et ssl_certificate_key.

Le port d'écoute du serveur est défini à travers l'option listen avec la variable {{ nginx_listen_port }}.

Le nom de domaine du serveur est défini à travers l'option server_name avec la variable {{ nginx_server_name }}.

### Vérifier que Nginx est correctement installé et configuré sur la machine virtuelle VM2.

Accéder au serveur Nginx en utilisant le nom de domaine spécifié dans la variable nginx_server_name dans le playbook Ansible. Le serveur devrait répondre aux requêtes HTTPS sur le port spécifié dans la variable nginx_listen_port.

La commande suivante permet également vérifier que le service tourne :

```bash
sudo systemctl status nginx
```




## Contributions
Les contributions sont les bienvenues! Pour contribuer, veuillez soumettre une pull request.

## Licence
Ce projet n'est sous aucune licence




### -------------------------------------------------------------------------------- ###



## Installation de docker Engine sur un hôte Ubuntu

Cette partie du readme consiste à utiliser Ansible pour installer Docker Engine sur un hôte Ubuntu 22.04. Docker Engine est un logiciel permettant de créer, déployer et exécuter des applications dans des conteneurs. Ansible est un outil open-source d'automatisation et de gestion de configuration qui permet de déployer des applications sur plusieurs serveurs de manière simultanée. 

### Prérequis:

Avant de commencer l'installation de Docker Engine avec Ansible, vous devez vous assurer que les éléments suivants sont installés sur votre machine de déploiement : 

- Ansible : vous pouvez l'installer en suivant les instructions de la documentation officielle d'Ansible. 
- Accès SSH à l'hôte Ubuntu 22.04 : assurez-vous que vous avez accès à l'hôte Ubuntu 

22\.04 en utilisant une connexion SSH avec un compte utilisateur disposant des droits d'administration. 

**Installation et exécution du projet :** 

1. Clonez le dépôt GitHub contenant le playbook Ansible : 

**git clone https://github.com/ A-Fon/Somily/docker\_ansible.git** 

2. Exécutez le playbook Ansible avec la commande suivante : **ansible-playbook -i hosts docker.yml** 
2. Vérification de l’installation de Docker Avec Hello World  

**sudo docker run hello-world** 

**Utilisation du projet :** 

Après avoir installé Docker Engine avec Ansible, vous pouvez l'utiliser pour déployer des applications dans des conteneurs sur l'hôte Ubuntu 22.04. Vous pouvez consulter la documentation officielle de Docker pour en savoir plus sur la création et le déploiement de conteneurs. 

**Déploiement de docker sur Ubuntu via Ansible :** 
```yaml

- name: Déployement de docker
  hosts: all
  become: yes

  tasks:


    - name: Mettre à jour le système
      become: true
      apt:
        update_cache: yes
        upgrade: dist

    - name: Installation des paquets ca-certificates, curl, gnupg
      apt:
        name:
        - ca-certificates
        - curl 
        - gnupg
        state: present
        update_cache: yes 

    - name: Création du dossier /etc/apt/keyrings
      file:
        path: /etc/apt/keyrings
        owner: 
        group: 
        mode: 0755
        recurse: true 

    - name: Ajouter la Clé GPG officielle de Docker
      apt_key:
        url: https://download.docker.com/linux/ubuntu/gpg
        keyring: /etc/apt/keyrings/docker.gpg

    - name: Configuration du dépôt
      shell:  echo "deb [arch="$(dpkg --print-architecture)" signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu "$(. /etc/os-release && echo "$VERSION_CODENAME")" stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
      
    - name: Installation du Docker Engine
      apt:
        name: 
        - docker-ce 
        - docker-ce-cli 
        - containerd.io 
        - docker-buildx-plugin 
        - docker-compose-plugin
        state: present
        update_cache: yes


    - name: Vérification avec hello world de l'installation que docker Engine est bien installé

      command: sudo docker run hello-world



    - name: Vérification que le conteneur Hello world fonctionne
      docker_container:
        image: hello-world
        name: hello-world
        state: started

    - name: Redemarrage du service docker?
      service:
        name: docker
        state: restarted
```

**Les tâches effectuées par ce playbook sont les suivantes :** 

- Mettre à jour le système 
- Installer les paquets nécessaires pour utiliser HTTPS d'APT : ca-certificates, curl et gnupg 
- Créer un dossier /etc/apt/keyrings dans lequel la clé GPG officielle de Docker sera déposée 
- Ajouter la clé GPG officielle de Docker 
- Configurer le dépôt de Docker 
- Installer Docker Engine qui contient plusieurs packages : docker-ce, docker-ce-cli, containerd.io, docker-buildx-plugin et docker-compose-plugin. 
- Vérifier l'installation de Docker Engine en exécutant le conteneur Hello World 
- Démarrer le conteneur Hello World pour vérifier qu'il fonctionne correctement 
- Redémarrer le service Docker





### -------------------------------------------------------------------------------- ###




## Déploiement de WordPress et d'une base de données MySQL sur une VM Ubuntu via un playbook Ansible

Cette partie est conçu pour déployer WordPress avec une base de données MySQL sur une machine virtuelle Ubuntu en utilisant Ansible et Docker.  Le déploiement est réalisé sur deux machines virtuelles, VM1 et VM2 avec une connexion SSH entre les deux, Le script Ansible est configuré pour installer Docker sur la machine virtuelle et créer un conteneur Docker pour WordPress et un autre pour MySQL. Ces conteneurs sont créés sur un réseau Docker isolé pour plus de sécurité. Les machines virtuelles doivent être sur le même réseau pour que la communication entre les deux soit possible. 

**Prérequis :** 

- Deux Machines virtuelles Ubuntu sur le même réseau 
- Une VM Ubuntu (VM2) qui sera utilisée pour héberger WordPress et la base de données MySQL. 
- Ansible doit être installé sur une autre machine (appelée VM1) pour exécuter le playbook. 
- Une connexion SSH entre la VM1 et la VM2. 

**Installation et exécution du projet :** 

**Étapes à suivre :** 

1. Assurez-vous que la machine virtuelle Ubuntu (VM2) est installée et opérationnelle. 
2. Clonez le repository contenant le playbook Ansible sur la machine virtuelle Ubuntu (VM1). 

**git clone <url du playbook>** 

3. Accéder au répertoire du Playbook en utilisant la commande  **cd <Chemin vers le playbook>** 
3. Modifier les variables du playbook 

Les variables du playbook doivent être modifiées selon les besoins. Les variables sont stockées dans le fichier ‘wordpress\_db.yml’. 

vars: 

`  `**db\_name**: wordpress 

`  `**db\_user**: wordpress 

`  `**db\_password**: strongpassword 

`  `**db\_password\_root**: strongbigpassword 

- **db\_name**: Le nom de la base de données MySQL à créer pour WordPress. 
- **db\_user**: Le nom d'utilisateur de la base de données MySQL pour WordPress. 
- **db\_password**: Le mot de passe de la base de données MySQL pour WordPress. 
- **db\_password\_root**: Le mot de passe root pour MySQL. 
5. Modifiez le fichier d'inventaire Ansible pour inclure l'adresse IP de la VM2  

**[db]** 

**137.74.106.208   Adresse IP ![](Aspose.Words.f4f660d7-7832-4974-a7f5-e998233dce0c.001.png)**

**[all:vars]** 

**ansible\_user=formation ansible\_become\_pass=AJCFormation6421!** 

6. Exécutez le playbook Ansible à l'aide de la commande :  **ansible-playbook -i hosts.ini wordpress\_db.yml** 

Le playbook va installer Docker sur la VM Ubuntu, créer un réseau Docker, démarrer une base de données MySQL et un serveur WordPress. 

**Utilisation du projet** 

Une fois que le playbook a été exécuté avec succès, vous pouvez accéder à WordPress en ouvrant un navigateur web et en naviguant vers l'adresse137.74.106.208  de la machine virtuelle Ubuntu (VM2) sur le port 80. Vous devriez être redirigé vers la page d'installation de WordPress. Et Suivez les instructions pour installer WordPress en utilisant la base de données MySQL créée par le playbook. 

Vous devriez maintenant avoir une instance de WordPress fonctionnelle avec une base de données MySQL sur la VM Ubuntu. Vous pouvez vous connecter à l'interface d'administration de WordPress pour vérifier l'installation. 

**Déploiement de WordPress et de MySQL avec Docker sur Ubuntu à l'aide d'Ansible** 

**Partie 1 : Installer Docker sur Ubuntu** 

```yaml
- name: Installer Docker sur Ubuntu ![](Aspose.Words.f4f660d7-7832-4974-a7f5-e998233dce0c.002.png)  hosts: all   tasks: 
  - name: Mettre à jour le système 

    become: true 

    apt: 

    update\_cache: yes 

    upgrade: dist 

- name: Installer Docker 

    become: true 

    apt: 

    name: docker.io 

    state: present 

- name: Ajouter l'utilisateur au groupe Docker 

    become: true 

    user: 

    name: "{{ ansible\_user }}" 

    groups: docker 

    append: yes 

- name: Redémarrer le service Docker 

    become: true 

    service: 

    name: docker 

    state: restarted 

```
**Description** 

Ce script est destiné à installer Docker sur une machine Ubuntu. Voici la signification de chaque tâche : 

1. La première tâche est appelée "Mettre à jour le système". Cette tâche utilise le module apt d'Ansible pour mettre à jour le cache des packages (update\_cache: yes) et mettre à jour le système (upgrade: dist). 
1. La deuxième tâche est appelée "Installer Docker". Cette tâche utilise à nouveau le module apt pour installer le package Docker (name: docker.io, state: present). 
1. La troisième tâche est appelée "Ajouter l'utilisateur au groupe Docker". Cette tâche utilise le module usermod pour ajouter l'utilisateur actuel ({{ ansible\_user }}) au groupe Docker (groups: docker, append: yes), ce qui permettra à l'utilisateur de lancer des commandes Docker sans avoir à utiliser sudo. 
4. La quatrième et dernière tâche est appelée "Redémarrer le service Docker". Cette tâche utilise le module systemctl pour redémarrer le service Docker (name: docker, state: restarted), de sorte que les changements effectués lors de l'installation de Docker soient pris en compte. 

**Partie 2 : Installer WordPress avec une base de données MySQL** 

```yaml
- name: Installer WordPress avec une base de données MySQL
  hosts: all
  become: true
  vars:
    db_name: wordpress
    db_user: wordpress
    db_password: strongpassword
    db_password_root: strongbigpassword
  tasks:
    - name: Créer un réseau Docker
      docker_network:
        name: wp_network



    - name: Démarrer une base de données MySQL
      docker_container:
        name: MySQL
        image: mysql
        state: started
        env:
          MYSQL_USER: "{{ db_user }}"
          MYSQL_PASSWORD: "{{ db_password }}"
          MYSQL_DATABASE: "{{ db_name }}"
          MYSQL_ROOT_PASSWORD: "{{ db_password_root }}"

        ports:
           - 3306:3306
        networks:
          - name: wp_network
            ipv4_address: 172.18.0.2




    - name: Démarrer un serveur WordPress
      docker_container:
        name: wordpress
        image: wordpress
        state: started
        env:
          WORDPRESS_DB_HOST: mysql:3306
          WORDPRESS_DB_NAME: "{{ db_name }}"
          WORDPRESS_DB_USER: "{{ db_user }}"
          WORDPRESS_DB_PASSWORD: "{{ db_password }}"
          WORDPRESS_DB_PASSWORD_ROOT: "{{ db_password_root }}"
        ports:
          - 80:80
        networks:
          - name: wp_network
            ipv4_address: 172.18.0.3
```

Le deuxième script est un script Ansible qui permet d'installer WordPress avec une base de données MySQL sur un réseau Docker. 

Le script commence par définir des variables qui seront utilisées pour la configuration de la base de données MySQL et du serveur WordPress. Les variables sont le nom de la base de données (db\_name), le nom d'utilisateur de la base de données (db\_user), le mot de passe de l'utilisateur de la base de données (db\_password), et le mot de passe root de la base de données (db\_password\_root). 

Ensuite, le script crée un réseau Docker appelé wp\_network en utilisant le module docker\_network d'Ansible. Ce réseau sera utilisé pour connecter les conteneurs de la base de données MySQL et du serveur WordPress. 

Ensuite, le script utilise le module docker\_container pour démarrer un conteneur MySQL. Ce conteneur est nommé MySQL, utilise l'image MySQL, et est configuré pour démarrer avec les variables d'environnement suivantes: MYSQL\_USER, MYSQL\_PASSWORD, MYSQL\_DATABASE, et MYSQL\_ROOT\_PASSWORD. Ces variables sont utilisées pour définir l'utilisateur MySQL, son mot de passe, le nom de la base de données, et le mot de passe root de la base de données. Le conteneur est également configuré pour écouter sur le port 3306 et est connecté au réseau wp\_network. 

Enfin, le script utilise à nouveau le module docker\_container pour démarrer un conteneur WordPress. Ce conteneur est nommé wordpress, utilise l'image WordPress, et est configuré pour démarrer avec les variables d'environnement suivantes: WORDPRESS\_DB\_HOST, WORDPRESS\_DB\_NAME, WORDPRESS\_DB\_USER, et WORDPRESS\_DB\_PASSWORD. Ces variables sont utilisées pour configurer la connexion de WordPress à la base de données MySQL. Le conteneur est également configuré pour écouter sur le port 80 et est connecté au réseau wp\_network. 

En résumé, ce script Ansible permet d'installer WordPress avec une base de données MySQL sur un réseau Docker en utilisant des conteneurs Docker. Le script crée un réseau Docker, démarre un conteneur MySQL pour la base de données, et démarre un conteneur WordPress pour le serveur web. 





### -------------------------------------------------------------------------------- ###





## Guide de déploiement de GitLab avec Docker sur une VM Ubuntu via un playbook Ansible 



Cette partie de script consiste à utiliser Docker Compose pour orchestrer des conteneurs Docker afin de créer et exécuter une instance GitLab Community Edition sur une machine virtuelle Ubuntu (VM2) à partir d'un playbook Ansible exécuté sur une autre machine virtuelle Ubuntu (VM1). 

**Prérequis** 

- Docker Compose installé sur votre machine locale 
- Une VM Ubuntu (VM2) qui sera utilisée pour héberger WordPress et la base de données MySQL. 
- Ansible doit être installé sur une autre machine (appelée VM1) pour exécuter le playbook. 

· 

**Étape 1 : Configurer la VM1** 

- Installez Ansible sur la VM1. (C’est déjà expliquer dans les autres parties) 
- Clonez le dépôt contenant le playbook Ansible :  
```
git clone[ https://github.com/ A-Fon/Somily/playbook_gitlab.git.](https://github.com/votre_utilisateur/playbook_gitlab.git) 
```
- Dans le fichier hosts, ajoutez l'adresse 137.74.106.203 de la VM2 sous le groupe [gitlab]. 

**Étape 2 : Configurer la VM2** 

- Installez Docker sur la VM2 en suivant les étapes décrites dans la réponse précédente. 
- Vérifiez que Docker est correctement installé en exécutant la commande docker run hello-world. 

Étape 3 : Exécuter le playbook Ansible 

- Dans le répertoire contenant le playbook Ansible, exécutez le playbook Ansible sur votre machine locale pour installer et configurer Docker et Docker Compose :  

**ansible-playbook playbook.yml -i hosts.** 

- Dans le dossier racine de ce référentiel, utilisez la commande suivante pour créer et exécuter le conteneur GitLab 

**docker-compose up –d** 

- Vérifiez que le conteneur GitLab a été créé et qu'il fonctionne correctement en exécutant la commande suivante : 

**docker-compose ps** 

**Utilisation du projet** 

1. Connectez-vous à l'adresse 137.74.106.203 de VM2 à l'aide de votre navigateur web. Vous devriez voir l'interface utilisateur de GitLab. 
2. Configurez GitLab selon vos besoins en suivant les instructions de configuration dans l'interface utilisateur. 

**Déploiement de GitLab** 

**Script utilisé :** 

```yaml
version: '3'

services:
  gitlab:
    image: gitlab/gitlab-ce:latest
    restart: always
    hostname: 137.74.106.203 #gitlab.example.com
    environment:
      GITLAB_OMNIBUS_CONFIG: |
        external_url 'http://137.74.106.203'
        gitlab_rails['gitlab_shell_ssh_port'] = 2222
    network_mode: host
    volumes:
      - ./config:/etc/gitlab
      - ./data:/var/opt/gitlab
      - ./logs:/var/log/gitlab

```


Il s'agit d'un fichier Docker Compose qui définit un service nommé "gitlab" qui exécute la dernière version de GitLab Community Edition (CE) à l'aide de l'image officielle GitLab Docker. 

- Le service est configuré pour redémarrer automatiquement s'il plante ou si le démon Docker est redémarré. Le nom d'hôte est défini sur l'adresse IP "137.74.106.203", qui est également accessible via le nom de domaine "gitlab.example.com". 
- Les variables d'environnement sont définies à l'aide de l'option "GITLAB\_OMNIBUS\_CONFIG", qui vous permet de configurer GitLab à l'aide du format de configuration Omnibus GitLab. Dans ce cas, l'URL externe pour GitLab est définie sur " http://137.74.106.203 " et le port SSH pour GitLab Shell est défini sur 2222. 
- Le mode réseau est défini sur "hôte", ce qui signifie que le conteneur partage l'espace de noms du réseau hôte, ce qui permet au conteneur d'utiliser la même adresse IP que l'hôte. Ceci est nécessaire pour permettre à GitLab d'écouter sur les ports 80 et 443. 
- Les volumes sont montés pour conserver la configuration, les données et les journaux GitLab en dehors du conteneur. "./config" est monté sur "/etc/gitlab", "./data" est monté sur "/var/opt/gitlab" et "./logs" est monté sur "/var/log/gitlab". 




### -------------------------------------------------------------------------------- ###




## Pour playbook prometheus grafana
## Prérequis
Une machine virtuelle Ubuntu avec Ansible installé et configuré pour se connecter à la machine distante.
Docker installé sur la machine distante.
## Description du playbook
Le playbook Ansible se compose de plusieurs tâches pour déployer Prometheus et Grafana via Docker Compose :
- Créer un dossier "prometheus" sur le serveur distant.
- Copie le fichier docker-compose.yml dans le dossier "prometheus".
- Copiera le fichier prometheus.yml dans le dossier "prometheus".
- Créera un dossier "grafana/provisioning/datasources/" dans le dossier "prometheus".
- Copiera le fichier prometheus_ds.yml dans le dossier "grafana/provisioning/datasources/".
- Créera un dossier "alertmanager" sur le serveur distant.
- Exécutera la commande "docker-compose up -d" dans le dossier "prometheus" pour lancer les conteneurs de Prometheus et Grafana.
## Configuration du playbook
Le playbook est configuré afin de déployer Prometheus et Grafana sur un hôte distant  à l’adresse IP 141.94.6.205.

Les fichiers de configuration pour Prometheus et Grafana sont stockés dans le répertoire files, et sont nommés prometheus.yml, docker-compose.yml et prometheus_ds.yml et sont mofiables.

### Configuration du fichier docker-compose.yml

Le fichier docker-compose.yml décrit l'ensemble des services Docker nécessaires pour déployer l'infrastructure de monitoring. Il est écrit en YAML et suit la syntaxe de Docker Compose.

Le dockers-compose.yml est découpé en plusieurs parties : la déclaration de la version du fichier, les volumes Docker permetant le stockage des données de manière persistante, et la partie avec les différents services (node-exporter, prometheus, grafana et alertmanager avec leur particularité).

```yaml
version: '3.9'
networks: 
  monitoring:
    driver: bridge

volumes:
  prometheus_data: {}
  grafana_data: {}
    
services:
  node-exporter: 
    image: prom/node-exporter:latest
    container_name: node-exporter
    restart: unless-stopped 
    volumes:
      - /proc:/host/proc:ro
      - /sys:/host/sys:ro
      - /:/rootfs:ro 
    command: 
    
      - '--path.procfs=/host/proc'
      - '--path.rootfs=/rootfs'
      - '--path.sysfs=/host/sys'
      - '--collector.filesystem.mount-points-exclude=^/(sys|proc|dev|host|etc)($$|/)'
    #expose: # a mettre lorsque l'on utilisera le reverse proxy
    #  - 9100
    ports:
      - 9100:9100
    deploy :
      mode: global 
    networks:
      - monitoring

  prometheus:
    image: prom/prometheus:latest
    container_name: prometheus
    restart: unless-stopped
    volumes:
      - ./prometheus.yml:/etc/prometheus/prometheus.yml
      - prometheus_data:/prometheus
    command:
      - '--config.file=/etc/prometheus/prometheus.yml'
      - '--storage.tsdb.path=/prometheus'
      - '--web.console.libraries=/etc/prometheus/console_libraries'
      - '--web.console.templates=/etc/prometheus/consoles'
      - '--web.enable-lifecycle'
    ports:
      - 9090:9090
    networks:
      - monitoring
   
  grafana:
    image: grafana/grafana:latest 
    container_name: grafana
    restart: unless-stopped
    volumes: 
      - grafana_data:/var/lib/grafana 
      - ./grafana/provisioning:/etc/grafana/provisioning 
    environment:
      - GF_SECURITY_ADMIN_USER=admin 
      - GF_SECURITY_ADMIN_PASSWORD=admin2023
      - GF_USERS_ALLOW_SIGN_UP=false
    ports:
      - 3000:3000
    depends_on:
      - prometheus
    networks:
      - monitoring

  alertmanager:
    image: prom/alertmanager:latest 
    container_name: alertmanager
    restart: unless-stopped
    volumes: 
      - ./alertemanager/:/etc/alertmanager/
    command:
      - '-config.file=/etc/alertmanager/config.yml'
      - '-storage.path=/alertmanager'
    ports:
      - 9093:9093
    deploy:
      mode: global
```

Le fichier docker-compose configure un réseau de surveillance avec les conteneurs Prometheus, Grafana, Node Exporter et Alertmanager. Les services sont déployés en mode global, ce qui signifie que chaque nœud du cluster aura une instance de ces services.

Le Node Exporter est un collecteur de métriques système pour les systèmes d'exploitation Unix. Il est déployé en mode global pour collecter les métriques de tous les nœuds du cluster.

Prometheus est un système de surveillance et d'alerte pour les applications. Il récupére et stocke les métriques collectées par Node Exporter.

Grafana est une plateforme d'analyse et de visualisation de données qui permet de créer des tableaux de bord à partir des métriques stockées dans Prometheus.

Alertmanager est un outil de gestion des alertes pour les applications. Il sert à envoyer des email en cas de détection de comportements anormaux dans les métriques collectées.
Les fichiers de configuration pour Prometheus, Grafana et Alertmanager sont stockés dans les volumes "prometheus_data", "grafana_data" et "./alertmanager/", respectivement.

Les ports exposés pour accéder aux services sont : 
- pour Node Exporter : 9100
- pour Prometheus : 9090  
- pour Grafana : 3000
- pour Alertmanager : 9093 

Le fichier de configuration de Prometheus  "./prometheus.yml"  et celuis d'Alertmanager "./alertmanager/config.yml"  sont montés en tant que volume pour le conteneur Prometheuset Alermanager respectivement. 

Le conteneur Grafana est configuré pour avoir un utilisateur administrateur avec les identifiants "admin" et "admin2023", cela peut être modifié.

L'enregistrement des nouveaux utilisateurs est désactivé avec la variable d'environnement "GF_USERS_ALLOW_SIGN_UP=false".
## D’autres configurations 
Pour configurer des sources de données supplémentaires ou des tableaux de bord personnalisés, il est possible de les ajouter en suivant les instructions de Grafana. 
Il est également possible personnaliser la configuration de Prometheus et Grafana en fonction des besoins. Les fichiers de configuration se trouvent dans les dossiers "prometheus" et "grafana" sur le serveur distant.

### Configuration du fichier prometheus.yml
Le fichier de configuration "prometheus.yml" est utilisé par Prometheus pour savoir quels endpoints doivent être scrappés et comment les données doivent être collectées. Il faut configurer des jobs de scrapping pour collecter des données auprès de différentes sources.
Voici comment configurer le fichier prometheus.yml pour scraper les données de différentes machines virtuelles :
L’ intervalle de scrapping de 15 secondes pour tous les jobs par défaut il est d’une minute.
```
global: scrape_interval: 15s
```
La section suivante, défini différents jobs de scrapping  pour collecter les données de différentes sources. Par exemple :
Le premier job ("prometheus") scrape les données du serveur Prometheus lui-même à partir de l'adresse IP 141.94.6.205:9090.
Le deuxième job ("wordpress") scrape les données d'un serveur WordPress à partir de l'adresse IP 137.74.106.208:9100.
Le troisième job ("nodeexporter") scrape les données d'un exporter de noeuds à partir de l'adresse IP 141.94.6.205:9100.

```yaml
scrape_configs:
- job_name: 'prometheus' 
    scrape_interval: 10s 
    static_configs:
        targets: ['141.94.6.205:9090']

- job_name: 'wordpress' 
    scrape_interval: 5s static_configs:
        targets: ['137.74.106.208:9100']

- job_name: 'nodeexporter' 
    scrape_interval: 5s static_configs:
        targets: ['141.94.6.205:9100']
```
Dans cette section, le gestionnaire d'alertes est défini pour envoyer des alertes à partir de Prometheus. Ici, le gestionnaire d'alertes utilisé est  "alertmanager" à l'adresse IP "alertmanager:9093".
```
alerting: alertmanagers:
scheme: http static_configs:
targets:
'alertmanager:9093'
```
### Configuration du fichier prometheus_ds.yml
Le fichier prometheus_ds.yml est un fichier de configuration de Grafana pour la source de données Prometheus, permettant à Grafana de communiquer avec le serveur Prometheus pour récupérer les données à visualiser.
Exemple du contenu du fichier :

```yaml
datasources:
- name: prometheus
  access: proxy
  type: prometheus
  url: http://prometheus:9090
  isDefault: true
```
Ce fichier contient une liste de sources de données. Les paramètres de cette source de données sont :
- ``name`` : le nom de la source de données
- ``access`` : le type d'accès à la source de données, dans ce cas "proxy" pour permettre à Grafana de faire une requête depuis le navigateur web de l'utilisateur
- ``type`` : le type de la source de données, dans ce cas "prometheus" pour indiquer que la source de données est un serveur Prometheus
- ``url`` : l'URL du serveur Prometheus, dans ce cas "http://prometheus:9090". Il est important de noter que "prometheus" est un alias défini dans le fichier docker-compose.yml qui permet à Grafana de communiquer avec le serveur Prometheus sans connaître son adresse IP.
- ``isDefault`` : un booléen indiquant si la source de données est la source de données par défaut de Grafana. Dans ce cas, c'est la source de données par défaut.

Ce fichier est placé dans le répertoire /prometheus/grafana/provisioning/datasources/ de la machine virtuelle sur laquelle Grafana est déployé. Lorsque Grafana est lancé, il scanne ce répertoire pour trouver des fichiers de configuration de sources de données. Si des fichiers sont trouvés, Grafana les utilise pour configurer les sources de données.


## Utilisation
Pour accéder à l'interface web de Grafana, il faut ouvrir un navigateur et utiliser l'adresse IP de l'hôte distant suivie du port 3000:
```
http://<IP de l'hôte distant>:3000
ici 
http://141.94.6.205:3000
```
Connectez-vous à Grafana avec les informations d'identification (admin/admin2023) et ajoutez une source de données Prometheus pour afficher les métriques collectées par Prometheus.





### -------------------------------------------------------------------------------- ###





# Déploiement d'Elasticsearch et de Kibana via un playbook Ansible utilisant un docker-compose.yml

Elasticsearch et Kibana sont des logiciels open-source qui sont couramment utilisés pour la recherche, l'analyse et la visualisation de données en temps réel. Elasticsearch est un moteur de recherche distribué qui peut être utilisé pour stocker et récupérer des données. Kibana est une interface Web qui permet de visualiser et d'analyser les données stockées dans Elasticsearch. Dans ce guide, nous allons installer Elasticsearch et Kibana via docker-compose sur un serveur Linux et le déployer à l'aide d'Ansible.

## Prérequis

Une machine virtuelle Linux avec Ansible installé et configuré pour se connecter à la machine distante.

Docker installé sur la machine distante.

## Déploiement d'Elasticsearch et Kibana

Lorsqu'Elasticsearch démarre pour la première fois, la configuration de sécurité suivante se produit automatiquement :

Des certificats et des clés sont générés pour les couches de transport et HTTP.
Les paramètres de configuration du Transport Layer Security (TLS) sont écrits dans elasticsearch.yml.
Un mot de passe est généré pour l'utilisateur elastic.
Un jeton d'inscription est généré pour Kibana.
Le mot de passe et le jeton d'inscription s'affichent sur le terminal.

Sous Docker, Kibana peut être configuré via des variables d'environnement.

## Configuration de Elasticsearch et de Kibana dans un docker-compose.

```yaml
version: '3.9'
networks: 
  monitoring:
    driver: bridge

volumes:
  elastic-data: {}
  kibana-data: {}
  certs: {}
 
services:
  elasticsearch: 
    image: docker.elastic.co/elasticsearch/elasticsearch:8.7.0 
    container_name: elasticsearch
    restart: unless-stopped 
    environment:
      - ELASTIC_PASSWORD=admin
      - discovery.type=single-node
      - node.name=database
      - bootstrap.memory_lock="true"
      - xpack.security.enabled="false"
    volumes:
      - elastic-data:/usr/share/elasticsearch/data
    ports:
      - 9200:9200
      - 9300:9300
    networks:
      - monitoring
    mem_limit: 1073741824 
    ulimits:
      memlock:
        soft: -1
        hard: -1
    healthcheck:
      test:
        [
          "CMD-SHELL",
          "curl -s http://localhost:9200 >/dev/null || exit 1",
        ]
      interval: 10s
      timeout: 10s
      retries: 120

  kibana:
    image: docker.elastic.co/kibana/kibana:8.7.0 
    container_name: kibana
    restart: unless-stopped 
    depends_on:
      elasticsearch:
        condition: service_healthy
    environment:
      - ELASTICSEARCH_HOSTS=http://elasticsearch:9200 
    mem_limit: 1073741824
    volumes:
      - certs:/usr/share/kibana/config/certs
      - kibana-data:/usr/share/kibana/data
    healthcheck:
      test:
        [
          "CMD-SHELL",
          "curl -s -I http://localhost:5601 | grep -q 'HTTP/1.1 302 Found'",
        ]
      interval: 10s
      timeout: 10s
      retries: 120

    ports:
      - 5601:5601
    networks:
      - monitoring
```

### Définition de certains termes des services Elasticsearch et Kibana
- ``mem_limit``: définit la limite de mémoire pour le conteneur, dans ce cas 1073741824 (soit 1 Go).
- ``ulimits``: définit les limites des ressources système pour le conteneur, dans ce cas memlock est configuré pour avoir des limites souples et dures à -1, ce qui signifie qu'elles sont illimitées.
- ``healthcheck``: définit le script de test pour vérifier si le conteneur est en bonne santé, l'interval de vérification, le timeout et le nombre de tentatives en cas d'échec.

## Configuration des variables d'environnement

Les variables d'environnement : 
- ``ELASTIC_PASSWORD=admin`` : définit le mot de passe pour le compte Elasticsearch "elastic". Par défaut, Elasticsearch est protégé par une authentification basique, où le compte "elastic" est utilisé pour administrer l'instance Elasticsearch. Dans ce cas, le mot de passe est défini sur "admin".
- ``discovery.type=single-node`` : indique à Elasticsearch de s'exécuter en mode "single-node", ce qui signifie qu'il s'agit d'une instance autonome sans autres nœuds dans le cluster Elasticsearch.
- ``node.name=database`` : définit le nom du nœud Elasticsearch, dans ce cas "database".
- ``bootstrap.memory_lock="true"`` : permet de verrouiller la mémoire de l'instance Elasticsearch en la chargeant en mémoire plutôt que d'utiliser la mémoire virtuelle, ce qui améliore les performances. Cette ligne active ce paramètre en le configurant à "true".
- ``xpack.security.enabled="false"`` : désactive la sécurité X-Pack d'Elasticsearch, qui fournit des fonctionnalités de sécurité telles que l'authentification, l'autorisation et l'audit. Dans ce cas, il est désactivé en le configurant à "false". Cependant, il est important de noter que cela réduit la sécurité de l'instance Elasticsearch, il est donc recommandé de le configurer correctement pour protéger l'instance et les données.
- ``ELASTICSEARCH_HOSTS`` : spécifie les URL des nœuds Elasticsearch auxquels l'application ou le service doit se connecter pour effectuer des requêtes ou récupérer des données. Dans ce cas, l'URL spécifiée est http://elasticsearch:9200, ce qui signifie que l'application ou le service se connectera à l'instance Elasticsearch qui s'exécute sur le même réseau Docker avec le nom de conteneur elasticsearch et le port 9200.

### Configuration du playbook Ansible pour le déployement

```yaml
- name: Installation de Elasticsearch et Kibana via docker-compose 
  become: true
  hosts: web


  tasks:
 
    - name: Création du dossier home/formation/elasticsearch sur le poste distant
      file:
        path: ./elasticsearch 
        owner: 
        group: 
        mode: 0755
        recurse: true 

    - name: Arrêter le conteneur s'il tourne
      command: docker compose down
      args:
         chdir: ./elasticsearch/    

    - name: Copie du fichier docker-compose.yml dans le dossier elasticsearch du poste distant
      copy:
        src: ./project_fil_rouge/docker/elasticsearch_kibana/docker-compose.yml
        dest: ./elasticsearch/docker-compose.yml 
        mode: 0755

    - name: Exécution du docker-compose
      command: docker compose up -d
      args:
        chdir: ./elasticsearch/
        
```

Ce playbook permet de réaliser de plusieurs étapes
- Etape 1 : la création d'un repertoire sur le poste distant
- Etape 2 :l'arrêt du conteneur s'il tourne
- Etape 3 :la copie de fichier docker-compose sur le poste distant 
- Etape 1 :l'exécution du docker-compose

# Utilisation du projet

A l'aide du navigateur et en utilisant l'adresse IP et le port associé, il est possible de se connecter aux différents services.

http://<IP de l'hôte distant>:9200 pour Elasticsearch
http://<IP de l'hôte distant>:5601 pour Kibana


### -------------------------------------------------------------------------------- ###



### OS 
#### Windows Server 2019 

L'OS Windows Server 2019 a été choisi principalement pour permettre l'utilisation de l'Active Directory qui est propre au serveur Windows et pour lequel il n'y a à ce jour pas d'équivalent sur le marché. L'Active Directory domaine serveur, permet la gestion des unités d'organisation, des groupes, des stratégies de groupes (GPO), des sites... Afin d'avoir une vue d'ensemble plus ergonomique, l'installation de Windows Server s'est fait avec interface graphique au lieu du mode core pour certains sites. L'AD-DS nécessitant l'utilisation du DNS, cette fonction lui a été attibuée et le DHCP aussi lui a été ajouté.

Rôles attribués au Windows Server 2019 :
- AD-DS : Active Directory Domain Server
- DNS : Domain Name Server, il permet d'associer un nom de domaine à une adresse IP (Internet Protocol)
- DHCP : Dynamic Host Configuration Protocol, fournit des adresses IP de façon automatique

    - Utilisation de PowerShell pour faire des scripts d'automatisation 


#### Windows Server 2019

3 VM Windows Server 2019 sont utilisées pour représenter les 3 sites de Somily (Paris, Rennes et Nantes), ces VM sont mises en place de sorte à ce que si l'un des serveurs tombe, les autres prennent le relais et que les informations soient répliquées.

Le téléchargement de Windows Serveur 2019 sur ce [lien](https://www.microsoft.com/fr-fr/evalcenter/download-windows-server-2019)

``Création de l'enveloppe, installation des Windows Server 2019``

Sur le navigateur, aller sur Machines Virtuelles, puis sur Créer/Enregistrer une machine virtuelle.

Les informations suivantes ont été entrées :

|Nom|Choix| WinSRV-PR|WinSVCore-RN|WinSRV-NT|
| :--- | :--- | :---: | :---: | :---: | 
|Compatibilité|Machine virtuelle ESXi 7.0|✔️ |✔️ |✔️ |
|Famille de systèmes d'exploitation invités|Windows|✔️ |✔️ |✔️ |
|Vision du SE invité|Microsoft Windows Server 2019 (64 bits)|✔️ |✔️ |✔️ |
|CPU|2 vCPUs|✔️ |✔️ |✔️ |
|Mémoire||4096 Mo|1024 Mo|4096 Mo|
|Disque dur 1|90 Go|✔️|✔️|✔️|
|Adaptateur réseau 1|Réseau LAN (Cocher connecté)|✔️|✔️|✔️|
|Lecteur de CD/DVD 1|&lt;ISO de Windows Server2019&gt; (Cocher connecter)|✔️|✔️|✔️|

En dehors de ces modifications tout le reste est laissé par défaut.

"Mettre sous tension" puis suivre ensuite les indications en fonction des préférences sur le fonctionnement futur des machines.

L'expérience de bureau correspond au fait d'avoir une interface graphique et est plus gourmande en ressources, raison pour laquelle la mémoire nécessaire est plus grande que sans.

|Nom| Choix| WinSRV-PR|WinSVCore-RN|WinSRV-NT|
| :--- | :--- | :---: | :---: | :---: | 
|Sélectionner le système d'exploitation à installer|Windows Server 2019 Datacenter Evaluation (expérience de bureau)|✔️|(Core)|✔️|

``Changement du nom des VM``

Sur le Gestionnaire de serveur aller sur Serveur local puis dans Nom de l'ordinateur et enfin modifier, puis entrer le nom correspondant. Cela permettra une meilleure visibilité concernant les VM utilisées lors des différentes configurations. Pour le Serveur Windows core, faire ``sconfig`` puis ``2`` (Nom d'ordinateur).

``

``Configuration du réseau et de l'adresse IP``

Fixer les adresses IP des serveurs, le masque de sous-réseau, la passerelle par défaut et le DNS

|Nom|WinSRV-PR|WinSVCore-RN|WinSRV-NT|
| :--- |:---: | :---: | :---: |
|IP|192.168.10.1|192.168.10.2|192.168.10.3|
|Masque de sous-réseau 255.255.255.0|✔️|✔️|✔️|
|Passerelle par défaut 192.168.10.4|✔️|✔️|✔️|
|Adresse DNS 192.168.10.1|✔️|✔️|✔️|

Pour cela suivre les chemins d'accés suivants en fonction du cas:

- Windows Server avec interface graphique :

  Ce PC ➡️ Réseau (Propriétés) ➡️ Modifier les paramètres de la carte ➡️ (Sur le Ethernet correspondant au réseau utilisé) (Propriétés)

    ➡️ Protocole Internet version 6 (TCP/IPv6) (décocher)

    ➡️ Protocole Internet version 4 (TCP/IPv4) (cocher) ➡️ (Mettre les informations ci-dessus)

- Windows Server Core :
  
```
sconfig
8 # Paramètres réseau
1 # Intel(R) 82574L Gigabit Network Connection
1 # Définir l'adresse de la carte réseau
s # Pour statique au lieu de dynamique 
192.168.10.2 # Adresse IP statique 
# rien car le masque par défaut est 255.255.255.0
192.168.10.4 # La même adresse de passerelle que WinSRV-PR
```

``Ajout des rôles AD-DS, DHCP et DNS``

|Nom|WinSRV-PR|WinSVCore-RN|WinSRV-NT|
| :--- |:---: | :---: | :---: |
|Rôle|Domaine Contrôleur principal |Domaine contrôleur secondaire|Domaine contrôleur en lecture seule (RODC)|

Ces ajouts peuvent se faire en même temps, en suivant le cheminement suivant.

Gestionnaire de serveur WinSRV-PR ➡️  Gérer  Ajouter des rôles et fonctionnalités ➡️  (laisser les informations par défaut) cocher DHCP, AD DS et DNS.

Promouvoir en tant que Domaine contrôleur principal pour l'AD-DS. Créer une nouvelle forêt nommée ``somily.local``.

Pour WinSRV-NT lors de la configuration de l'AD-DS cocher "Contrôleur de domaine en lecture seule (RODC)". L'intérêt du RODC est d'avoir les informations d'un contrôleur de domaine classique dans les mots de passe utilisateurs et que toutes les informations sont stockées en lecture seule, cela permet notamment de sécuriser les sites distants, d'avoir les requêtes du DNS de mise en cache et d'économiser la bande passante lors de l'authentification des utilisateurs (employés, stagiaires et élèves pour Somily).

Pour WinSVCore-RN

``` PowerShell
$ PowerShell
$ Get-ExecutionPolicy
$ Set-ExecutionPolicy RemoteSigned
$ Get-ExecutionPolicy
$ Get-Module
$ Import-Module ServerManager
$ Get-Module
$ Install-WindowsFeature AD-Domain-Services -IncludeManagementTools
$ Install-ADDSDomainController -DomainName "Somily.local" -InstallDns:$true -Credential (Get-Credential "SOMILY\administrateur")
$ O
$ Get-WindowsFeature # Vérifier si l'AD DS est bien installé
$ ipconfig /all
```
L'ajout d'un second contrôleur de domaine AD, permet que si le premier contrôleur de domaine n'est pas joignable, ce second contrôleur s'occupera de prendre le relais en gérant tout ce que le premier aurait dû faire. C'est un moyen d'assurer la haute disponibilité.


